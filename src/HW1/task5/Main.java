package HW1.task5;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        try {
            int n = inputN();
            System.out.println("Успешный ввод!");
        }
        catch (Exception exc) {
            System.err.println(exc);
        }
    }

    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner con = new Scanner(System.in);
        int n = con.nextInt();
        if (n >= 100 || n <= 0) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }
}
