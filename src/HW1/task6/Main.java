package HW1.task6;

public class Main {
    public static void main(String[] args) throws CheckNameException, CheckBirthdateException, CheckGenderException, CheckHeightException {
        FormValidator.checkName("Duke");
        FormValidator.checkHeight("180");
        FormValidator.checkBirthdate("01/01/2000");
        FormValidator.checkGender("Female");
    }
}
