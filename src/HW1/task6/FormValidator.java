package HW1.task6;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class FormValidator {
    public static void checkName(String str) throws CheckNameException {
        if (str.length() < 2 || str.length() > 20 || !(str.substring(0, 1).equals(str.substring(0, 1).toUpperCase()))) throw new CheckNameException();
    }

    public static void checkBirthdate(String str) throws CheckBirthdateException {
        Date minDate = new Date(Date.parse("01/01/1900"));
        Date birthdate = new Date(Date.parse(str));
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        Date currentDate = new Date(Date.parse(date.format(formatter)));
        if (minDate.compareTo(birthdate) > 0 || currentDate.compareTo(birthdate) < 0) throw new CheckBirthdateException();//
    }

    public static void checkGender(String str) throws CheckGenderException {
        enum Gender {Male, Female};
        Gender male = Gender.Male;
        Gender female = Gender.Female;
        if (!(str.equals(male.toString()) || str.equals(female.toString()))) throw new CheckGenderException();
    }

    public static void checkHeight(String str) throws CheckHeightException {
        if (!(Double.parseDouble(str) > 0)) throw new CheckHeightException();
    }
}
