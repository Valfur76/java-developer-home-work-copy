package HW1.task6;

public class CheckGenderException extends Exception {
    @Override
    public String toString() {
        return "Gender is not correct";
    }
}
