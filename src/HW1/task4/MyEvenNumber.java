package HW1.task4;

public final class MyEvenNumber {
    int n;
    public MyEvenNumber(int n) throws OddNumberException  {
         this.n = setN(n);
    };

    public class OddNumberException extends Exception {
        @Override
        public String toString() {
            return "OddNumberException";
        }
    }

    public int setN(int n) throws OddNumberException {
        if (n%2 != 0) {
            throw new OddNumberException();
        }
        else return n;
    }
}
