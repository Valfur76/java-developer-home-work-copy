package HW1.task3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        readChangeWriteFile();
    }

    private static void readChangeWriteFile() {
        StringBuilder f = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader("./input.txt"))) {
            String line;
            while((line = in.readLine()) != null) {
                f.append(line + "\n");
            }
            System.out.println("Успешное чтение");
        }
        catch (IOException e) {
            System.err.println(e + "Ошибка чтения файла");
        }

        boolean b = false;
        try(FileWriter sw = new FileWriter("./output.txt", b)) {
            sw.write(changeCase(f.toString()));
            if(!b) System.out.println("Запись сохранена");
        }
        catch(IOException e) {
            System.err.println(e + "Ошибка записи файла");
        }
    }

    public static String changeCase(String str) {
            return str.toUpperCase();
    }
}
