package HW1.task8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n;
        Scanner con = new Scanner(System.in);
        n = con.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = con.nextInt();
        }
        int p;
        p = con.nextInt();
        System.out.println(getIndex(arr, p, 0, arr.length-1));
    }

    public static int getIndex(int[] arr, int p, int i_begin, int i_end) {
        int index = arr.length/2+1;
        if (p == arr[index]) {
            return index;
        }
        else if (p < index) {
                return getIndex(arr, p, i_begin, index);
             }
             else {
                 return getIndex(arr, p, index, i_end);
             }
    }
}
